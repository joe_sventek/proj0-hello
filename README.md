# Proj0-Hello
# author: Joe Sventek
# email: jsventek@uoregon.edu
-------------

This project is quite simple - the program hello/hello.py obtains a message
from a file named "credentials.ini" (which is not part of the repository) which must reside in the hello directory. When invoked via "make run" in the repository main directory, the program obtains the value associated with the attribute "message" in credentials.ini and prints it on standard output.
